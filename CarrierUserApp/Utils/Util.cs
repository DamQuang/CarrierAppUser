﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarrierUserApp.Utils
{
    public class Util
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static string DateFromString(DateTime dateTime)
        {
            return dateTime.ToString("dd MMM yyy HH:mm:ss GMT");
        }
        public static string ShortDateFromString(DateTime dateTime)
        {
            return dateTime.ToString("dd MMM yyy");
        }
        public static string ShortTimeString(DateTime dateTime)
        {
            return dateTime.ToString("HH:mm:ss");
        }
        public static long ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalSeconds;
        }
        public static DateTime ConvertTimestampToDateTime(long unixTimeStamp)
        {
            //var timeSpan = TimeSpan.FromSeconds(unixTimeStamp);
            //var localDateTime = new DateTime(timeSpan.Ticks).ToLocalTime();
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
