﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarrierUserApp.Models
{
    public class RegisterInfo
    {
        public KeyUser UserRegister { get; set; }
    }
    public class RegisterInfoApi : ApiResponse<RegisterInfo>
    {

    }
    public class KeyUser
    {
        public string PrivateKey { get; set; }
        public string UserId { get; set; }

    }
}
