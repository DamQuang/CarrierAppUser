﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarrierUserApp.Models
{
    public class VanDon
    {
        public int Id { get; set; }
        public double Gia { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }
        public double SoKmUocTinh { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public DateTime ThoiGianDi { get; set; }
        public object ThoiGianDen { get; set; }
        public string UserName { get; set; }
        public int LoaiThanhToan { get; set; }
        public string MaVanDon { get; set; }
        public DateTime CreateAt { get; set; }
        public object UpdateAt { get; set; }
        public double FromLat { get; set; }
        public double FromLng { get; set; }
        public double ToLat { get; set; }
        public double ToLng { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Lenght { get; set; }
        public bool VAT { get; set; }
        public double TrongLuong { get; set; }
        public string DienThoaiLienHe { get; set; }
        public string Created_By { get; set; }
        public string NguoiLienHe { get; set; }
        public string ParentId { get; set; }
        public string TrackingStatus { get; set; }
        public List<object> DiemTraHang { get; set; }
        public object License { get; set; }
        public object Payload { get; set; }
        public object DriverName { get; set; }
    }

    public class SubPerson
    {
        public string Phone { get; set; }
        public string Name { get; set; }
    }

    public class VanDonDetail
    {
        public VanDon Order { get; set; }
        public SubPerson SubPerson { get; set; }
    }

}
