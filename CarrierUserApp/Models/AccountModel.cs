﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarrierUserApp.Models
{
    public class AccountModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FullName { get; set; }
        public string MobilePhone { get; set; }
        public string Address { get; set; }
    }
}
