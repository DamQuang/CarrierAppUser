﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidHUD;
using CarrierUser.Droid.Util;
using CarrierUserApp;
using CarrierUserApp.Droid;
using CarrierUserApp.Models;
using Newtonsoft.Json;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace CarrierUser.Droid.Activities
{
    [Activity(Label = "DetailActivity")]
    public class DetailActivity : Activity
    {
        public OrderDetail order;
        DataService service;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            order = JsonConvert.DeserializeObject<OrderDetail>(Intent.GetStringExtra("order_detail"));
            SetContentView(Resource.Layout.Detail);
            var toolbar = FindViewById<Toolbar>(Resource.Id.app_bar_Detail);
            var btnBack = toolbar.FindViewById<ImageView>(Resource.Id.btnBack_Detail);
            btnBack.Click += delegate
            {
                Finish();
            };
            AndHUD.Shared.Show(this, "Loading...", -1, MaskType.Black);

            Task.Run(async () =>
            {
                try
                {
                    service = new DataService(CarrierUserAppDroid.loginInfo.CurrentToken.access_token);
                    var vandondetail = await service.GetOrderDetail(order.Id);
                    if (vandondetail != null)
                    {
                        RunOnUiThread(() =>
                        {
                            var orderInfo = vandondetail.Result.Order;
                            var driverInfo = vandondetail.Result.SubPerson;
                            var txtMaVanDon = FindViewById<TextView>(Resource.Id.txtMavandon);
                            var txtTenHang = FindViewById<TextView>(Resource.Id.txtTenhang);
                            var diemdi = FindViewById<TextView>(Resource.Id.txtDiemdi_Detail);
                            var diemden = FindViewById<TextView>(Resource.Id.txtDiemden_Detail);
                            var quangduong = FindViewById<TextView>(Resource.Id.txtQuangduong);
                            var thoigiandi = FindViewById<TextView>(Resource.Id.txtThoigiandi);
                            //var thoigianden = FindViewById<TextView>(Resource.Id.txtThoigianden);
                            var gia = FindViewById<TextView>(Resource.Id.txtGia);
                            var loaithanhtoan = FindViewById<TextView>(Resource.Id.txtPay);
                            var trangthai = FindViewById<TextView>(Resource.Id.txtTrackingStatus);
                            var tentaixe = FindViewById<TextView>(Resource.Id.txtName_driver);
                            var bsx = FindViewById<TextView>(Resource.Id.txtLicense_plates);
                            var trongtai = FindViewById<TextView>(Resource.Id.txtKhoiLuongXe);

                            var linearLayoutThongTinXe = FindViewById<LinearLayout>(Resource.Id.linearThongTinXe);

                            if (orderInfo.DriverName != null)
                            {
                                tentaixe.Text = orderInfo.DriverName.ToString();
                                bsx.Text = orderInfo.License.ToString();
                                trongtai.Text = string.Format("{0} {1}", orderInfo.Payload, " tấn");
                            }
                            else
                            {
                                linearLayoutThongTinXe.Visibility = ViewStates.Gone;
                            }

                            txtMaVanDon.Text = orderInfo.MaVanDon;
                            txtTenHang.Text = orderInfo.TenHang;
                            diemdi.Text = orderInfo.DiemDiChiTiet;
                            diemden.Text = orderInfo.DiemDenChiTiet;
                            quangduong.Text = orderInfo.SoKmUocTinh.ToString();
                            thoigiandi.Text = orderInfo.ThoiGianDi.ToString();
                            //thoigianden.Text = orderInfo.ThoiGianDen;
                            gia.Text = orderInfo.Gia.ToString();
                            loaithanhtoan.Text = orderInfo.LoaiThanhToan.ToString();
                            if (orderInfo.TrackingStatus != null)
                            {
                                if (int.Parse(orderInfo.TrackingStatus) != 0)
                                {
                                    trangthai.Text = Utitlitys.StatusToString(int.Parse(orderInfo.TrackingStatus));
                                }
                                else
                                {
                                    trangthai.Text = Utitlitys.StatusToString(orderInfo.Status);
                                }
                            }
                            else
                            {
                                trangthai.Text = Utitlitys.StatusToString(orderInfo.Status);
                            }
                        });
                    }
                    else
                    {

                    }
                    AndHUD.Shared.Dismiss(this);

                }
                catch (Exception ex)
                {
                    AndHUD.Shared.Dismiss(this);
                    RunOnUiThread(() =>
                    {
                        Toast.MakeText(this, ex.Message, ToastLength.Long);
                    });
                }

            });
        }
    }
}