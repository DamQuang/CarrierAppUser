﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidHUD;
using CarrierUser.Droid.Util;
using CarrierUserApp;
using CarrierUserApp.Models;

namespace CarrierUser.Droid.Activities
{
    [Activity(Label = "RegisterActivity")]
    public class RegisterActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            DataStorage dataStorage = new DataStorage(FileAccessHelper.GetRootFolderPath());
            SetContentView(Resource.Layout.Register);
            var txtUserNameRegister = FindViewById<EditText>(Resource.Id.txtUserNameRegister);
            var txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
            var txtConfirmPass = FindViewById<EditText>(Resource.Id.txtConfirmPass);
            var txtFullName = FindViewById<EditText>(Resource.Id.txtFullName);
            var txtPhone = FindViewById<EditText>(Resource.Id.txtPhone);
            var txtAddress = FindViewById<EditText>(Resource.Id.txtAddress);
            var btnRegister = FindViewById<Button>(Resource.Id.btnRegister);
            var btnBack = FindViewById<ImageView>(Resource.Id.btnBack_Register);
            btnRegister.Click += async delegate
            {
                AndHUD.Shared.Show(this, "verifying...", -1, MaskType.Black);
                DataService dataService = new DataService(null);

                AccountModel accBody = new AccountModel();
                accBody.UserName = txtUserNameRegister.Text;
                accBody.Password = txtPassword.Text;
                accBody.ConfirmPassword = txtConfirmPass.Text;
                accBody.MobilePhone = txtPhone.Text;
                accBody.Address = txtAddress.Text;
                accBody.FullName = txtFullName.Text;
                var registerInfo = await dataService.RegisterReg(accBody);
                if (registerInfo != null)
                {
                    if (registerInfo.Message.Equals("Success"))
                    {
                        goLoginScreen();
                        Toast.MakeText(this, "Đăng kí tài khoản thành công", ToastLength.Long).Show();
                    }
                    else if (registerInfo.Message.Equals("Tài khoản đã được đăng ký!"))
                    {
                        Toast.MakeText(this, "Tài khoản đã được đăng ký! Mời đăng ký lại.", ToastLength.Long).Show();
                    }
                    else
                    {
                        Toast.MakeText(this, registerInfo.Message, ToastLength.Long).Show();
                    }
                }
                AndHUD.Shared.Dismiss(this);
                dataStorage.SaveData(registerInfo,"RegisterInfo");
                //var loginInfo = await dataService.Login(txtUserName.Text, txtPass.Text);
                //if (loginInfo != null)
                //{
                //    var deviceToken = CarrierUserAppDroid.DeviceToken();
                //    if (!string.IsNullOrEmpty(deviceToken))
                //    {
                //        dataService = new DataService(loginInfo.auth.auth_token);
                //        await dataService.RegisterPushNotification(deviceToken, loginInfo.user.identityId);
                //    }
                //}
                //dataStorage.SaveData(loginInfo, "LoginInfo");
                //CarrierUserAppDroid.loginInfo = loginInfo;
                //AndHUD.Shared.Dismiss(this);
                //goHomeScreen();
            };
            btnBack.Click += async delegate
            {
                Finish();
            };
        }
        void goHomeScreen()
        {
            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }
        void goLoginScreen()
        {
            var intent = new Intent(this, typeof(LoginActivity));
            
            StartActivity(intent);
            Toast.MakeText(this, "Đăng kí tài khoản thành công", ToastLength.Long).Show();
            Finish();
        }
    }
}